#include<iostream>
#include<string.h>
using namespace std;

class Laptop{
	private:
		int code;
		string name;
		int ram;
		int hdd;	
	public:
	
	void setLaptop(int pc_code,string pc_name,int pc_ram,int pc_hdd){
		this->code=pc_code;
		this->name=pc_name;
		ram=pc_ram;
		hdd=pc_hdd;
	}
	
	void getComputer(){	
		cout<<code<<"-"<<name<<" - "<<ram<<"G - "<<hdd<<"G"<<endl; 
	}
	
	Laptop(){
		this->code=8;
		this->name="Asus";
		ram=12;
		hdd=200;
	}
	
	Laptop(int pc_code,string pc_name,int pc_ram,int pc_hdd){		
		this->code=pc_code;
		this->name=pc_name;
		ram=pc_ram;
		hdd=pc_hdd;
	}
		
};

int main(){
	
	//create object
	Laptop pc1=Laptop();
	Laptop pc2=Laptop(03,"PC3",4,300);
	Laptop pc3;
	
	//pc1.setLaptop(01,"Dall",8,500);
//	pc2.setLaptop(02,"Asus",4,128);
	pc3.setLaptop(03,"Lenovo",16,512);
	
	pc1.getComputer();
	pc2.getComputer();
	pc3.getComputer();
	
	
	
	
	return(0);
}